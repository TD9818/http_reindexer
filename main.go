// https://github.com/shehab-as/Go-Microservices/blob/main/cmd/main.go
// sudo docker run -p9088:9088 -p6534:6534 -p16534:16534 -it reindexer/reindexer

package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"strings"

	"main/internal/businessblock"
	"main/internal/ctrls"
	"main/internal/db_reindexer"
	"main/internal/globals"
)

// Глобальные переменные, считанные из conf.yaml
var IS_DOCKER bool = globals.IS_DOCKER
var DOCKER_CONTAINER string = globals.DOCKER_CONTAINER
var IS_REPORT bool = globals.IS_REPORT

//var db_stor server_utils.DB_storage

func TaskHandler(controllersItem ctrls.ControllerItem) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {

		err := controllersItem.ItemService.ItemRepo.DBClient.Ping()
		if err != nil {
			http.Error(w, err.Error(), http.StatusMethodNotAllowed)
			http.Error(w, fmt.Sprintln("Ошибка соединения с Reindexer!"), http.StatusMethodNotAllowed)
			return
		}
		var rr = ctrls.ResReq{w, req}
		path := strings.Trim(req.URL.Path, "/")
		pathParts := strings.Split(path, "/")
		method_ := req.Method

		switch method_ {
		case http.MethodDelete:
			switch pathParts[1] {
			case "doc0":
				controllersItem.DelDoc0(rr)
			case "doc1":
				controllersItem.DelDoc1(rr)
			case "doc2":
				controllersItem.DelDoc2(rr)
			case "doc0all":
				controllersItem.DelDoc0All(rr)
			case "doc1all":
				controllersItem.DelDoc1All(rr)
			case "doc2all":
				controllersItem.DelDoc2All(rr)
			case "doc_all":
				controllersItem.DelDocsTotal(rr)
			default:
				fmt.Fprintf(w, fmt.Sprintf(
					"Необходимы команды: 'doc0', 'doc1', 'doc2', 'doc0all', 'doc1all', 'doc2all', 'doc_all', а не %v",
					pathParts[1]))
			}

		case http.MethodPost:

			switch pathParts[1] {
			case "random_set":
				controllersItem.CreateRandomDocs(rr)

			case "create_doc0":
				controllersItem.CreateDoc0(rr)

			case "create_doc1":
				controllersItem.CreateDoc1(rr)

			case "create_doc2":
				controllersItem.CreateDoc2(rr)

			default:
				fmt.Fprintf(w, fmt.Sprintf(
					"Необходимы команды: 'random_set', 'create_doc0', 'create_doc1', 'create_doc2', а не %v",
					pathParts[1]))
			}

		case http.MethodGet:

			switch pathParts[1] {
			case "get_doc0_page":
				//  Если нужен полный список уровня 0, Get-параметр: lim_str должен отсутствовать
				controllersItem.GetDoc0Page(rr)

			case "get_doc1_page":
				controllersItem.GetDoc1Page(rr)

			case "get_doc2_page":
				controllersItem.GetDoc2Page(rr)

			case "jdoc0_sort_page":
				controllersItem.GetCacheJDoc0SortPage(rr)

			case "jdoc1_sort_page":
				controllersItem.GetCacheJDoc1SortPage(rr)

			case "get_cache_doc2":
				controllersItem.Get_cache_doc2(rr)

			default:
				msg1 := fmt.Sprintf("Необходимы команды: 'get_doc0_page', 'get_doc1_page', 'get_doc2_page', ")
				msg2 := fmt.Sprintf("'jdoc1_sort_page', 'jdoc0_sort_page', 'get_cache_doc2', а не %v", pathParts[1])
				fmt.Fprintf(w, msg1+msg2)
			}

		case http.MethodPatch:
			switch pathParts[1] {
			case "update_doc0":
				controllersItem.UpdateDoc0(rr)

			case "update_doc1":
				controllersItem.UpdateDoc1(rr)

			case "update_doc2":
				controllersItem.UpdateDoc2(rr)

			default:
				fmt.Fprintf(w, fmt.Sprintf("Необходимы команды: 'update_doc0', 'update_doc1', 'update_doc2', а не %v", pathParts[1]))
			}

		default:
			fmt.Fprintf(w, fmt.Sprintf("Разрешенные методы: POST, DELETE, UPDATE, PATCH, а не %v", method_))
		}
	}
}

func enviromentHandler(w http.ResponseWriter, req *http.Request) {
	svp := os.Getenv("SERVERPORT")
	if svp == "" {
		fmt.Fprint(w, "Переменная окружения 'SERVERPORT' пустая или не считана!!!\n")
		fmt.Fprint(w, "Обращайтесь на порт по умолчанию - ':4112'\n\n")
	} else {
		fmt.Fprintf(w, fmt.Sprintf("Переменная окружения 'SERVERPORT' = %v\n\n", svp))
	}
	fmt.Fprintf(w, fmt.Sprintf("Глобальная переменная 'DOCKER_CONTAINER' = %v, type %T\n\n", DOCKER_CONTAINER, DOCKER_CONTAINER))
}

func yamlHandler(w http.ResponseWriter, r *http.Request) {

	var c globals.Conf

	c.GetConf(w)

	js, err := json.Marshal(c)
	if err != nil {
		msg := fmt.Sprintf("json.Marshal: %v", err.Error())
		http.Error(w, msg, http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}

func getHandler(w http.ResponseWriter, r *http.Request) {

	query := r.URL.Query()
	for i, param := range query {
		fmt.Fprintf(w, fmt.Sprintf("%v - %T - %v - %T\n", i, i, param, param))
	}
	if query.Get("aaa") == "" {
		fmt.Fprintf(w, "Нет значения для aaa")
	} else {
		fmt.Fprintf(w, fmt.Sprintf("aaa - %v\n", query.Get("aaa")))
	}
}

func yamltoglobalsHandler(w http.ResponseWriter, r *http.Request) {

	fmt.Fprintf(w, "Глобальная переменная IS_DOCKERL, взятая из test.yaml = %v \n", IS_DOCKER)
	fmt.Fprintf(w, "Глобальная переменная IS_REPORTL, взятая из test.yaml = %v \n", IS_REPORT)
	fmt.Fprintf(w, "Глобальная переменная DOCKER_CONTAINER, взятая из test.yaml = %v \n", DOCKER_CONTAINER)
}

func main() {

	db_conn := db_reindexer.New()
	defer db_conn.Close()
	repoItem := db_reindexer.NewReindItem(db_conn)
	businessItem := businessblock.NewServiceItem(repoItem)
	controllerItem := ctrls.NewControllerItem(businessItem)

	http.HandleFunc("/new/", TaskHandler(*controllerItem))
	http.HandleFunc("/env/", enviromentHandler)
	http.HandleFunc("/yaml/", yamlHandler)
	http.HandleFunc("/yamltoglobals/", yamltoglobalsHandler)
	http.HandleFunc("/get_params/", getHandler)

	svp := os.Getenv("SERVERPORT")
	if svp == "" {
		svp = "4112"
	}
	svad := ":" + svp
	fmt.Printf("\nSERVERADDR = %v\n", svad)

	http.ListenAndServe(svad, nil)

}
