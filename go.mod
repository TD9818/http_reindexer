module main

go 1.19

require (
	github.com/restream/reindexer v4.5.0+incompatible
	gopkg.in/yaml.v2 v2.4.0
)

require (
	github.com/golang/snappy v0.0.4 // indirect
	github.com/hashicorp/golang-lru v0.5.4 // indirect
	github.com/iancoleman/orderedmap v0.2.0 // indirect
)
