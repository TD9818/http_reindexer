package businessblock

import (
	"fmt"
	"net/url"
	"strconv"
)

type D interface{}

type Docfields struct {
	Hashid         string
	Id             int
	Title          string
	Text           string
	Tags           []string
	Year           int
	Month          int
	Day            int
	Docshashidlist []string
}

func (d *Docfields) GetDocfields(query url.Values) *Docfields {

	res, err := strconv.Atoi(query.Get("Id"))
	if err == nil {
		d.Id = res
	}
	res1 := query.Get("Title")
	if res1 != "" {
		d.Title = res1
	}
	res1 = query.Get("Text")
	if res1 != "" {
		d.Text = res1
	}
	res1 = query.Get("Tags")
	if res1 != "" {
		d.Tags = Str_to_mass(res1)
	}
	res, err = strconv.Atoi(query.Get("Year"))
	if err == nil {
		d.Year = res
	}
	res, err = strconv.Atoi(query.Get("Month"))
	if err == nil {
		d.Month = res
	}
	res, err = strconv.Atoi(query.Get("Day"))
	if err == nil {
		d.Day = res
	}
	res1 = query.Get("Docshashidlist")
	if res1 != "" {
		d.Docshashidlist = Str_to_mass(res1)
	}
	res1 = query.Get("Hashid")
	if res1 != "" {
		d.Hashid = res1
	} else {
		d.Hashid = MD5(fmt.Sprintf("%d%v", d.Id, d.Title))
	}
	return d
}
