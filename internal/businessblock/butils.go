package businessblock

import (
	"crypto/md5"
	"fmt"
	"math/rand"

	// "main/internal/globals"
	"strings"
)

func Str_to_mass(s string) []string {

	s_ := strings.Trim(s, "]")
	s_ = strings.Trim(s_, "[")
	mass := []string{}
	for _, item := range strings.Split(s_, ",") {
		mass = append(mass, strings.Trim(item, " "))
	}
	return mass
}

func MD5(data string) string {
	// MD5 - Превращает содержимое из переменной data в md5-хеш

	h := md5.Sum([]byte(data))
	return fmt.Sprintf("%x", h)
}

func Zirnd(i int, len_str int) string {
	// ZeroIntegerRandom
	// Генерит случайное целое число от 0 до i - 1,
	// Дополняет его с переди нулями
	// и возвращает ввиде строки длиной len_str

	num := rand.Int() % i
	len_num := len(fmt.Sprintf("%v", num))
	len_ := len_str - len_num - 1
	return fmt.Sprintf("00000000000000000000000%v", num)[22-len_:]
}

func Is_str_in_mass(s string, mass []string) bool {
	// Возвращает true, если s есть в массиве строк mass, иначе возвращает false.

	res := false
	for _, vol := range mass {
		if s == vol {
			res = true
			break
		}
	}
	return res
}

func Is_int_in_mass(s int, mass []int) bool {
	// Возвращает true, если s есть в массиве чисел mass, иначе возвращает false.

	res := false
	for _, vol := range mass {
		if s == vol {
			res = true
			break
		}
	}
	return res
}
