package businessblock

import (
	"fmt"
	"main/internal/db_reindexer"
	"main/internal/globals"
	"main/internal/types"
	"math/rand"
	"net/url"
)

var DOC0NAMESPACE string = globals.DOC0NAMESPACE
var DOC1NAMESPACE string = globals.DOC1NAMESPACE
var DOC2NAMESPACE string = globals.DOC2NAMESPACE

type ServiceItem struct {
	ItemRepo *db_reindexer.ReindItem
}

func NewServiceItem(itemRepo *db_reindexer.ReindItem) *ServiceItem {

	// db_reindexer.OpenNamespace(itemRepo.DBClient, DOC0NAMESPACE, types.Document_0{})
	// db_reindexer.OpenNamespace(itemRepo.DBClient, DOC1NAMESPACE, types.Document_1{})
	// db_reindexer.OpenNamespace(itemRepo.DBClient, DOC2NAMESPACE, types.Document_2{})

	return &ServiceItem{
		ItemRepo: itemRepo,
	}
}

func (d_ *ServiceItem) CreateDoc0(query url.Values) (types.Document_0, error) {
	// Create a new doc in the store.

	dfields := Docfields{}
	dfields.GetDocfields(query)

	new_doc := types.Document_0{}
	new_doc.Hashid = MD5(fmt.Sprintf("%d%v", dfields.Id, dfields.Title))
	new_doc.Id = dfields.Id
	new_doc.Title = dfields.Title
	new_doc.Text = dfields.Text
	new_doc.Tags = dfields.Tags
	new_doc.Year = dfields.Year
	new_doc.Month = dfields.Month
	new_doc.Day = dfields.Day
	new_doc.Docs1hashidlist = dfields.Docshashidlist
	new_doc.Docs1list = []*types.Document_1{}

	res, err := d_.ItemRepo.CreateDoc(types.Document_0{}, new_doc, DOC0NAMESPACE)

	doc := *res.(*types.Document_0)

	return doc, err
}

func (d_ *ServiceItem) CreateDoc1(query url.Values) (types.Document_1, error) {
	// Create a new doc in the store.

	dfields := Docfields{}
	dfields.GetDocfields(query)

	new_doc := types.Document_1{}
	new_doc.Hashid = MD5(fmt.Sprintf("%d%v", dfields.Id, dfields.Title))
	new_doc.Id = dfields.Id
	new_doc.Title = dfields.Title
	new_doc.Text = dfields.Text
	new_doc.Tags = dfields.Tags
	new_doc.Year = dfields.Year
	new_doc.Month = dfields.Month
	new_doc.Day = dfields.Day
	new_doc.Docs2hashidlist = dfields.Docshashidlist
	new_doc.Docs2list = []*types.Document_2{}

	res, err := d_.ItemRepo.CreateDoc(types.Document_1{}, new_doc, DOC1NAMESPACE)
	doc := *res.(*types.Document_1)
	return doc, err
}

func (d_ *ServiceItem) CreateDoc2(query url.Values) (types.Document_2, error) {
	// Create a new doc in the store.

	dfields := Docfields{}
	dfields.GetDocfields(query)

	new_doc := types.Document_2{}
	new_doc.Hashid = MD5(fmt.Sprintf("%d%v", dfields.Id, dfields.Title))
	new_doc.Id = dfields.Id
	new_doc.Title = dfields.Title
	new_doc.Text = dfields.Text
	new_doc.Tags = dfields.Tags
	new_doc.Year = dfields.Year
	new_doc.Month = dfields.Month
	new_doc.Day = dfields.Day

	res, err := d_.ItemRepo.CreateDoc(types.Document_2{}, new_doc, DOC2NAMESPACE)
	doc := *res.(*types.Document_2)
	return doc, err
}

func (d_ *ServiceItem) CreateRandomDocs(n0 int, n1 int, n2 int) {
	// Создание партии случайных документов, распределенных по каталогам

	if n0 < 0 {
		n0 = -n0
	}
	if n1 < 0 {
		n1 = -n1
	}
	if n2 < 0 {
		n2 = -n2
	}
	// Создаем для doc0 случайные параметры
	docs0_list := map[int]*types.Document_0{}
	for i := 0; i < n0; i++ {
		new_doc := types.Document_0{}
		new_doc.Id = 100 + (rand.Int() % 899)
		new_doc.Title = fmt.Sprintf("Doc0Title_%v", Zirnd(999, 4))
		new_doc.Hashid = MD5(fmt.Sprintf("%d%v", new_doc.Id, new_doc.Title))
		new_doc.Text = fmt.Sprintf("Doc0tDescript_%v_Doc0Descript", Zirnd(999, 6))
		new_doc.Tags = []string{"a", "b"}
		new_doc.Year = 2000 + rand.Int()%22
		new_doc.Month = (rand.Int() % 12) + 1
		new_doc.Day = (rand.Int() % 30) + 1
		new_doc.Docs1hashidlist = []string{}
		new_doc.Docs1list = []*types.Document_1{}

		docs0_list[i] = &new_doc
	}
	// Создаем для doc1 случайные параметры
	docs1_list := map[int]*types.Document_1{}
	for i := 0; i < n1; i++ {
		new_doc1 := types.Document_1{}
		new_doc1.Id = 100 + (rand.Int() % 899)
		new_doc1.Title = fmt.Sprintf("Doc1tTitle_%v", Zirnd(999, 4))
		new_doc1.Hashid = MD5(fmt.Sprintf("%d%v", new_doc1.Id, new_doc1.Title))
		new_doc1.Text = fmt.Sprintf("Doc1Descript_%v_Doc1Descript", Zirnd(999, 6))
		new_doc1.Tags = []string{"s", "g"}
		new_doc1.Year = 1900 + rand.Int()%22
		new_doc1.Month = (rand.Int() % 12) + 1
		new_doc1.Day = (rand.Int() % 30) + 1
		new_doc1.Docs2hashidlist = []string{}
		new_doc1.Docs2list = []*types.Document_2{}

		docs1_list[i] = &new_doc1
	}
	// Создаем doc2 со случайными параметрами
	for i := 0; i < n2; i++ {
		new_doc2 := types.Document_2{}
		new_doc2.Id = 100 + (rand.Int() % 899)
		new_doc2.Title = fmt.Sprintf("Doc2Title_%v", Zirnd(999, 4))
		new_doc2.Hashid = MD5(fmt.Sprintf("%d%v", new_doc2.Id, new_doc2.Title))
		new_doc2.Text = fmt.Sprintf("Doc2tDescript_%v_Doc2Descript", Zirnd(999, 6))
		new_doc2.Tags = []string{"w", "o"}
		new_doc2.Year = 1800 + rand.Int()%22
		new_doc2.Month = (rand.Int() % 12) + 1
		new_doc2.Day = (rand.Int() % 30) + 1

		_, err := d_.ItemRepo.CreateDoc(types.Document_2{}, new_doc2, DOC2NAMESPACE)
		// Учебный пример!!!
		//fmt.Sprint("%+v", *elem.(*types.Document_2)) // Расшифровка элемента типа interface типом Document2

		if err == nil {
			n := rand.Int() % n1
			docs1_list[n].Docs2hashidlist = append(docs1_list[n].Docs2hashidlist, new_doc2.Hashid)
		}
	}
	// Создаем doc1
	for _, new_doc1 := range docs1_list {
		_, err := d_.ItemRepo.CreateDoc(types.Document_1{}, new_doc1, DOC1NAMESPACE)
		if err == nil {
			n := rand.Int() % n0
			docs0_list[n].Docs1hashidlist = append(docs0_list[n].Docs1hashidlist, new_doc1.Hashid)
		}
	}
	// Создаем doc0
	for _, new_doc0 := range docs0_list {
		d_.ItemRepo.CreateDoc(types.Document_0{}, new_doc0, DOC0NAMESPACE)
	}
}

func (d_ *ServiceItem) readDoc0(docHashId string) (types.Document_0, error) {

	elem, err := d_.ItemRepo.ReadDoc(docHashId, types.Document_0{}, DOC0NAMESPACE)
	if err == nil {
		return *elem.(*types.Document_0), nil
	}
	return types.Document_0{}, err
}

func (d_ *ServiceItem) readDoc1(docHashId string) (types.Document_1, error) {

	elem, err := d_.ItemRepo.ReadDoc(docHashId, types.Document_1{}, DOC1NAMESPACE)
	if err == nil {
		return *elem.(*types.Document_1), nil
	}
	return types.Document_1{}, err
}

func (d_ *ServiceItem) readDoc2(docHashId string) (types.Document_2, error) {

	elem, err := d_.ItemRepo.ReadDoc(docHashId, types.Document_2{}, DOC2NAMESPACE)
	if err == nil {
		return *elem.(*types.Document_2), nil
	}
	return types.Document_2{}, err
}

func (d_ *ServiceItem) readCacheDoc0(docHashId string) (types.Document_0, error) {

	elem, err := d_.ItemRepo.ReadCacheDoc(docHashId, types.Document_0{}, DOC0NAMESPACE)
	if err == nil {
		doc := *elem.(*types.Document_0)
		return types.Document_0{
			Hashid:          doc.Hashid,
			Id:              doc.Id,
			Title:           doc.Title,
			Text:            doc.Text,
			Tags:            doc.Tags,
			Year:            doc.Year,
			Month:           doc.Month,
			Day:             doc.Day,
			Docs1hashidlist: doc.Docs1hashidlist,
			Docs1list:       doc.Docs1list,
		}, nil
	}
	return types.Document_0{}, err
}

func (d_ *ServiceItem) readCacheDoc1(docHashId string) (types.Document_1, error) {

	elem, err := d_.ItemRepo.ReadCacheDoc(docHashId, types.Document_1{}, DOC1NAMESPACE)
	if err == nil {
		doc := *elem.(*types.Document_1)
		return types.Document_1{
			Hashid:          doc.Hashid,
			Id:              doc.Id,
			Title:           doc.Title,
			Text:            doc.Text,
			Tags:            doc.Tags,
			Year:            doc.Year,
			Month:           doc.Month,
			Day:             doc.Day,
			Docs2hashidlist: doc.Docs2hashidlist,
			Docs2list:       doc.Docs2list,
		}, nil
	}
	return types.Document_1{}, err
}

func (d_ *ServiceItem) ReadCacheDoc2(docHashId string) (types.Document_2, error) {

	elem, err := d_.ItemRepo.ReadCacheDoc(docHashId, types.Document_2{}, DOC2NAMESPACE)
	if err == nil {
		doc := *elem.(*types.Document_2)
		return types.Document_2{
			Hashid: doc.Hashid,
			Id:     doc.Id,
			Title:  doc.Title,
			Text:   doc.Text,
			Tags:   doc.Tags,
			Year:   doc.Year,
			Month:  doc.Month,
			Day:    doc.Day,
		}, nil
	}
	return types.Document_2{}, err
}

func (d_ *ServiceItem) UpdateDoc0(docHashId string, change_list []string, query url.Values) (int, error) {

	doc, err := d_.readDoc0(docHashId)
	if err != nil {
		return 0, err
	}

	dfields := Docfields{}
	dfields.GetDocfields(query)

	if Is_str_in_mass("Text", change_list) {
		doc.Text = dfields.Text
	}
	if Is_str_in_mass("Tags", change_list) {
		doc.Tags = dfields.Tags
	}
	if Is_str_in_mass("Year", change_list) {
		doc.Year = dfields.Year
	}
	if Is_str_in_mass("Month", change_list) {
		doc.Month = dfields.Month
	}
	if Is_str_in_mass("Day", change_list) {
		doc.Day = dfields.Day
	}
	if Is_str_in_mass("Docshashidlist", change_list) {
		doc.Docs1hashidlist = dfields.Docshashidlist
	}

	return d_.ItemRepo.UpdateDoc(doc, types.Document_0{}, DOC0NAMESPACE)
}

func (d_ *ServiceItem) UpdateDoc1(docHashId string, change_list []string, query url.Values) (int, error) {

	doc, err := d_.readDoc1(docHashId)
	if err != nil {
		return 0, err
	}

	dfields := Docfields{}
	dfields.GetDocfields(query)

	if Is_str_in_mass("Text", change_list) {
		doc.Text = dfields.Text
	}
	if Is_str_in_mass("Tags", change_list) {
		doc.Tags = dfields.Tags
	}
	if Is_str_in_mass("Year", change_list) {
		doc.Year = dfields.Year
	}
	if Is_str_in_mass("Month", change_list) {
		doc.Month = dfields.Month
	}
	if Is_str_in_mass("Day", change_list) {
		doc.Day = dfields.Day
	}
	if Is_str_in_mass("Docshashidlist", change_list) {
		doc.Docs2hashidlist = dfields.Docshashidlist
	}

	return d_.ItemRepo.UpdateDoc(doc, types.Document_1{}, DOC1NAMESPACE)
}

func (d_ *ServiceItem) UpdateDoc2(docHashId string, change_list []string, query url.Values) (int, error) {

	doc, err := d_.readDoc2(docHashId)
	if err != nil {
		return 0, err
	}

	dfields := Docfields{}
	dfields.GetDocfields(query)

	if Is_str_in_mass("Text", change_list) {
		doc.Text = dfields.Text
	}
	if Is_str_in_mass("Tags", change_list) {
		doc.Tags = dfields.Tags
	}
	if Is_str_in_mass("Year", change_list) {
		doc.Year = dfields.Year
	}
	if Is_str_in_mass("Month", change_list) {
		doc.Month = dfields.Month
	}
	if Is_str_in_mass("Day", change_list) {
		doc.Day = dfields.Day
	}

	return d_.ItemRepo.UpdateDoc(doc, types.Document_2{}, DOC2NAMESPACE)
}

func (d_ *ServiceItem) DelDoc2(docHashId string) (int, error) {

	return d_.ItemRepo.DelDoc(docHashId, types.Document_2{}, DOC2NAMESPACE)
}

func (d_ *ServiceItem) DelDoc1(docHashId string) (int, error) {

	doc1, err := d_.readCacheDoc1(docHashId)
	if err == nil {
		for _, hashid := range doc1.Docs2hashidlist {
			d_.DelDoc2(hashid)
		}
		return d_.ItemRepo.DelDoc(docHashId, types.Document_1{}, DOC1NAMESPACE)
	}
	return 0, err
}

func (d_ *ServiceItem) DelDoc0(docHashId string) (int, error) {

	doc0, err := d_.readCacheDoc0(docHashId)
	if err == nil {
		for _, hashid := range doc0.Docs1hashidlist {
			d_.DelDoc1(hashid)
		}
		return d_.ItemRepo.DelDoc(docHashId, types.Document_0{}, DOC0NAMESPACE)
	}
	return 0, err
}

func (d_ *ServiceItem) DelDoc0All() (int, error) {
	return d_.ItemRepo.DelDocAll(types.Document_0{}, DOC0NAMESPACE)
}

func (d_ *ServiceItem) DelDoc1All() (int, error) {
	return d_.ItemRepo.DelDocAll(types.Document_1{}, DOC1NAMESPACE)
}

func (d_ *ServiceItem) DelDoc2All() (int, error) {
	return d_.ItemRepo.DelDocAll(types.Document_2{}, DOC2NAMESPACE)
}

func (d_ *ServiceItem) DelDocTotal_() ([3]int, error) {
	n0, err0 := d_.ItemRepo.DelDocAll(types.Document_0{}, DOC0NAMESPACE)
	n1, err1 := d_.ItemRepo.DelDocAll(types.Document_1{}, DOC1NAMESPACE)
	n2, err2 := d_.ItemRepo.DelDocAll(types.Document_2{}, DOC2NAMESPACE)
	if n0 == 0 && n1 == 0 && n2 == 0 && (err0 != nil || err1 != nil || err2 != nil) {
		return [3]int{0, 0, 0}, nil
	}
	return [3]int{n0, n1, n2}, nil
}

func (d_ *ServiceItem) ReadDocs0Page_(http_query url.Values) ([]types.Document_0, error) {
	return d_.ItemRepo.ReadDocs0Page(http_query, DOC0NAMESPACE, false)
}

func (d_ *ServiceItem) readDocs0All(http_query url.Values) ([]types.Document_0, error) {
	return d_.ItemRepo.ReadDocs0Page(http_query, DOC0NAMESPACE, true)
}

func (d_ *ServiceItem) ReadDocs1Page_(http_query url.Values) ([]types.Document_1, error) {
	return d_.ItemRepo.ReadDocs1Page(http_query, DOC1NAMESPACE, false)
}

func (d_ *ServiceItem) readDocs1All(http_query url.Values) ([]types.Document_1, error) {
	return d_.ItemRepo.ReadDocs1Page(http_query, DOC1NAMESPACE, true)
}

func (d_ *ServiceItem) ReadDocs2Page_(http_query url.Values) ([]types.Document_2, error) {
	return d_.ItemRepo.ReadDocs2Page(http_query, DOC2NAMESPACE, false)
}

func (d_ *ServiceItem) readDocs2All(http_query url.Values) ([]types.Document_2, error) {
	return d_.ItemRepo.ReadDocs2Page(http_query, DOC2NAMESPACE, true)
}

func (d_ *ServiceItem) ReadCacheJDoc1SortPage_(HashId string, http_query url.Values) (types.JDoc1SortPage, error) {
	return d_.ItemRepo.ReadCacheJDoc1SortPage(HashId, DOC1NAMESPACE, DOC2NAMESPACE, http_query)
}

func (d_ *ServiceItem) ReadCacheJDoc0SortPage_(HashId string, http_query url.Values) (types.JDoc0SortPage, error) {
	return d_.ItemRepo.ReadCacheJDoc0SortPage(HashId, DOC0NAMESPACE, DOC1NAMESPACE, DOC2NAMESPACE, http_query)
}
