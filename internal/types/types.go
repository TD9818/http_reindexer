package types

type Document_0 struct { // Документ 0-го уровня
	Hashid          string        `json:"Hashid"          reindex:"Hashid,hash,pk"`
	Id              int           `json:"Id"              reindex:"Id"`
	Title           string        `json:"Title"           reindex:"Title"` //
	Text            string        `json:"Text"            reindex:"Text"`
	Tags            []string      `json:"Tags"            reindex:"Tags"`
	Year            int           `json:"Year"            reindex:"Year"`  // Срок обязательного хранения
	Month           int           `json:"Month"           reindex:"Month"` // Период верификации
	Day             int           `json:"Day"             reindex:"Day"`   // Срок существования пустого документа
	Docs1hashidlist []string      `json:"Docs1hashidlist" reindex:"Docs1hashidlist"`
	Docs1list       []*Document_1 `reindex:"Docs1list,,joined"`
}

type Document_1 struct { // Документ 1-го уровня
	Hashid          string        `json:"Hashid"          reindex:"Hashid,hash,pk"`
	Id              int           `json:"Id"              reindex:"Id"`
	Title           string        `json:"Title"           reindex:"Title"` //
	Text            string        `json:"Text"            reindex:"Text"`
	Tags            []string      `json:"Tags"            reindex:"Tags"`
	Year            int           `json:"Year"            reindex:"Year"`
	Month           int           `json:"Month"           reindex:"Month"`
	Day             int           `json:"Day"             reindex:"Day"`
	Docs2hashidlist []string      `json:"Docs2hashidlist" reindex:"Docs2hashidlist"`
	Docs2list       []*Document_2 `reindex:"Docs2list,,joined"`
}

type Document_2 struct { // Документ 2-го уровня
	Hashid string   `json:"Hashid" reindex:"Hashid,hash,pk"`
	Id     int      `json:"Id"     reindex:"Id"`
	Title  string   `json:"Title"  reindex:"Title"` //
	Text   string   `json:"Text"   reindex:"Text"`
	Tags   []string `json:"Tags"   reindex:"Tags"`
	Year   int      `json:"Year"   reindex:"Year"`
	Month  int      `json:"Month"  reindex:"Month"`
	Day    int      `json:"Day"    reindex:"Day"`
}

type Doc0Strip struct { // Загодовок 0-го уровня связанного документа
	Hashid string   `json:"Hashid"          reindex:"Hashid,hash,pk"`
	Id     int      `json:"Id"              reindex:"Id"`
	Title  string   `json:"Title"           reindex:"Title"` //
	Text   string   `json:"Text"            reindex:"Text"`
	Tags   []string `json:"Tags"            reindex:"Tags"`
	Year   int      `json:"Year"            reindex:"Year"`  // Срок обязательного хранения
	Month  int      `json:"Month"           reindex:"Month"` // Период верификации
	Day    int      `json:"Day"             reindex:"Day"`   // Срок существования пустого документа
}

type Doc1Strip struct { // Загодовок 1-го уровня связанного документа
	Hashid string   `json:"Hashid"          reindex:"Hashid,hash,pk"`
	Id     int      `json:"Id"              reindex:"Id"`
	Title  string   `json:"Title"           reindex:"Title"` //
	Text   string   `json:"Text"            reindex:"Text"`
	Tags   []string `json:"Tags"            reindex:"Tags"`
	Year   int      `json:"Year"            reindex:"Year"`  // Срок обязательного хранения
	Month  int      `json:"Month"           reindex:"Month"` // Период верификации
	Day    int      `json:"Day"             reindex:"Day"`   // Срок существования пустого документа
}

type JDoc0SortPage struct { // Структура связанного документа с сортированным постраничным содержимым
	Doc0     Doc0Strip       // Загодовок 0-го уровня связанного документа
	Doc1List []JDoc1SortPage // Страница сортированного списка вложенных документов 1-го уровня
}

type JDoc1SortPage struct { // Структура связанного документа с сортированным постраничным содержимым
	Doc1     Doc1Strip    // Загодовок 1-го уровня связанного документа
	Doc2List []Document_2 // Страница сортированного списка вложенных документов 2-го уровня
}
