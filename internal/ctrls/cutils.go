package ctrls

import (
	"encoding/json"
	"fmt"
	"net/http"
)

func SendMsgNotAllowedMetods(rr ResReq) {
	// Ответ о разрешенных методах
	msg := fmt.Sprintf("expect method GET, DELETE, PATH or POST at /task/, got %v", rr.Req.Method)
	http.Error(rr.W, msg, http.StatusMethodNotAllowed)
}

func JsonResponse(W http.ResponseWriter, obj obj_interface) {
	js, err := json.Marshal(obj)
	if err != nil {
		http.Error(W, err.Error(), http.StatusInternalServerError)
		return
	}
	W.Header().Set("Content-Type", "application/json")
	W.Write(js)

}
