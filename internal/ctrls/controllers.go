package ctrls

import (
	"fmt"
	"strconv"

	"main/internal/businessblock"
)

type ControllerItem struct {
	ItemService *businessblock.ServiceItem
}

func NewControllerItem(itemService *businessblock.ServiceItem) *ControllerItem {
	return &ControllerItem{
		ItemService: itemService,
	}
}

func (i_ ControllerItem) CreateDoc0(rr ResReq) {

	res, err := i_.ItemService.CreateDoc0(rr.Req.URL.Query())
	if err != nil {
		JsonResponse(rr.W, err.Error())
	}
	JsonResponse(rr.W, res)
}

func (i_ ControllerItem) CreateDoc1(rr ResReq) {

	res, err := i_.ItemService.CreateDoc1(rr.Req.URL.Query())
	if err != nil {
		JsonResponse(rr.W, err.Error())
	}
	JsonResponse(rr.W, res)
}

func (i_ ControllerItem) CreateDoc2(rr ResReq) {

	res, err := i_.ItemService.CreateDoc2(rr.Req.URL.Query())
	if err != nil {
		JsonResponse(rr.W, err.Error())
	}
	JsonResponse(rr.W, res)
}

func (i_ ControllerItem) CreateRandomDocs(rr ResReq) {

	var n_0 int
	var n_1 int
	var n_2 int
	query := rr.Req.URL.Query()

	n_0_str := query.Get("n_docs_0")
	if n_0_str == "" {
		n_0 = 0
	} else {
		res, err := strconv.Atoi(n_0_str)
		if err == nil {
			n_0 = res
		} else {
			n_0 = 0
		}
	}
	n_1_str := query.Get("n_docs_1")
	if n_1_str == "" {
		n_1 = 0
	} else {
		res, err := strconv.Atoi(n_1_str)
		if err == nil {
			n_1 = res
		} else {
			n_1 = 0
		}
	}
	n_2_str := query.Get("n_docs_2")
	if n_2_str == "" {
		n_2 = 0
	} else {
		res, err := strconv.Atoi(n_2_str)
		if err == nil {
			n_2 = res
		} else {
			n_2 = 0
		}
	}

	i_.ItemService.CreateRandomDocs(n_0, n_1, n_2)
	JsonResponse(rr.W, fmt.Sprintf("Операция с параметрами: 0-й уровень - %vшт., 1-й уровень - %vшт., 2-й уровень %vшт. - произведена", n_0, n_1, n_2))
}

func (i_ ControllerItem) DelDoc0(rr ResReq) {

	hashid := rr.Req.URL.Query().Get("hashid")
	if hashid != "" {
		msg, err := i_.ItemService.DelDoc0(hashid)
		if err != nil {
			JsonResponse(rr.W, err.Error())
		}
		JsonResponse(rr.W, fmt.Sprintln(msg))
	}
	JsonResponse(rr.W, fmt.Sprintln("Get-параметр hashid не найден!"))
}

func (i_ ControllerItem) DelDoc1(rr ResReq) {

	hashid := rr.Req.URL.Query().Get("hashid")
	if hashid != "" {
		msg, err := i_.ItemService.DelDoc1(hashid)
		if err != nil {
			JsonResponse(rr.W, err.Error())
		}
		JsonResponse(rr.W, fmt.Sprintln(msg))
	}
	JsonResponse(rr.W, fmt.Sprintln("Get-параметр hashid не найден!"))
}

func (i_ ControllerItem) DelDoc2(rr ResReq) {

	hashid := rr.Req.URL.Query().Get("hashid")
	if hashid != "" {
		n, err := i_.ItemService.DelDoc2(hashid)
		if err != nil {
			JsonResponse(rr.W, err.Error())
		}
		JsonResponse(rr.W, fmt.Sprintf("Удалено %v документов 2-го уровня", n))
	}
	JsonResponse(rr.W, fmt.Sprintln("Get-параметр hashid не найден!"))
}

func (i_ ControllerItem) DelDoc0All(rr ResReq) {

	n, err := i_.ItemService.DelDoc0All()
	if err != nil {
		JsonResponse(rr.W, err.Error())
	}
	JsonResponse(rr.W, fmt.Sprintf("Удалено %v документов 0-го уровня с вложениями", n))
}

func (i_ ControllerItem) DelDoc1All(rr ResReq) {

	n, err := i_.ItemService.DelDoc0All()
	if err != nil {
		JsonResponse(rr.W, err.Error())
	}
	JsonResponse(rr.W, fmt.Sprintf("Удалено %v документов 1-го уровня с вложениями", n))
}

func (i_ ControllerItem) DelDoc2All(rr ResReq) {

	n, err := i_.ItemService.DelDoc0All()
	if err != nil {
		JsonResponse(rr.W, err.Error())
	}
	JsonResponse(rr.W, fmt.Sprintf("Удалено %v документов 2-го уровня", n))
}

func (i_ ControllerItem) DelDocsTotal(rr ResReq) {
	res, err := i_.ItemService.DelDocTotal_()
	if err != nil {
		JsonResponse(rr.W, err.Error())
	}
	JsonResponse(rr.W, fmt.Sprintf("Удаление документов: 0-й уровень - %vшт., 1-й уровень - %vшт., 2-й уровень - %vшт. - произведено", res[0], res[1], res[2]))
}

func (i_ ControllerItem) GetDoc0Page(rr ResReq) {
	res, err := i_.ItemService.ReadDocs0Page_(rr.Req.URL.Query())
	if err != nil {
		JsonResponse(rr.W, err.Error())
	}
	JsonResponse(rr.W, res)
}

func (i_ ControllerItem) GetDoc1Page(rr ResReq) {
	res, err := i_.ItemService.ReadDocs1Page_(rr.Req.URL.Query())
	if err != nil {
		JsonResponse(rr.W, err.Error())
	}
	JsonResponse(rr.W, res)
}

func (i_ ControllerItem) GetDoc2Page(rr ResReq) {
	res, err := i_.ItemService.ReadDocs2Page_(rr.Req.URL.Query())
	if err != nil {
		JsonResponse(rr.W, err.Error())
	}
	JsonResponse(rr.W, res)
}

func (i_ ControllerItem) GetCacheJDoc1SortPage(rr ResReq) {
	HashId := rr.Req.URL.Query().Get("hashid")
	if HashId == "" {
		JsonResponse(rr.W, "Get-параметр hashid не найден!")
	}
	res, err := i_.ItemService.ReadCacheJDoc1SortPage_(HashId, rr.Req.URL.Query())
	if err != nil {
		JsonResponse(rr.W, err.Error())
	}
	JsonResponse(rr.W, res)
}

func (i_ ControllerItem) GetCacheJDoc0SortPage(rr ResReq) {
	HashId := rr.Req.URL.Query().Get("hashid")
	if HashId == "" {
		JsonResponse(rr.W, "Get-параметр hashid не найден!")
	}
	res, err := i_.ItemService.ReadCacheJDoc0SortPage_(HashId, rr.Req.URL.Query())
	if err != nil {
		JsonResponse(rr.W, err.Error())
	}
	JsonResponse(rr.W, res)
}

func (i_ ControllerItem) UpdateDoc0(rr ResReq) {
	// Допустимые для изменения данных поля:
	// "Text", "Tags", "Year", "Month", "Day", "Docshashidlist",

	change_list := []string{}
	for key, _ := range rr.Req.URL.Query() {
		change_list = append(change_list, key)
	}
	hashid := rr.Req.URL.Query().Get("hashid")
	if hashid == "" {
		fmt.Fprintf(rr.W, fmt.Sprintln("Get-параметр hashid не найден!"))
	}
	res, err := i_.ItemService.UpdateDoc0(hashid, change_list, rr.Req.URL.Query())
	if err != nil {
		JsonResponse(rr.W, err.Error())
	}
	JsonResponse(rr.W, fmt.Sprintf("Колличество обновленных документов doc0: %v", res))
}

func (i_ ControllerItem) UpdateDoc1(rr ResReq) {
	// Допустимые для изменения данных поля:
	// "Text", "Tags", "Year", "Month", "Day", "Docshashidlist",

	change_list := []string{}
	for key, _ := range rr.Req.URL.Query() {
		change_list = append(change_list, key)
	}
	hashid := rr.Req.URL.Query().Get("hashid")
	if hashid == "" {
		fmt.Fprintf(rr.W, fmt.Sprintln("Get-параметр hashid не найден!"))
	}
	res, err := i_.ItemService.UpdateDoc1(hashid, change_list, rr.Req.URL.Query())
	if err != nil {
		JsonResponse(rr.W, err.Error())
	}
	JsonResponse(rr.W, fmt.Sprintf("Колличество обновленных документов doc1: %v", res))
}

func (i_ ControllerItem) UpdateDoc2(rr ResReq) {
	// Допустимые для изменения данных поля:
	// "Text", "Tags", "Year", "Month", "Day", "Docshashidlist",

	change_list := []string{}
	for key, _ := range rr.Req.URL.Query() {
		change_list = append(change_list, key)
	}
	hashid := rr.Req.URL.Query().Get("hashid")
	if hashid == "" {
		fmt.Fprintf(rr.W, fmt.Sprintln("Get-параметр hashid не найден!"))
	}
	res, err := i_.ItemService.UpdateDoc2(hashid, change_list, rr.Req.URL.Query())
	if err != nil {
		JsonResponse(rr.W, err.Error())
	}
	JsonResponse(rr.W, fmt.Sprintf("Колличество обновленных документов doc2: %v", res))
}

func (i_ ControllerItem) Get_cache_doc2(rr ResReq) {

	HashId := rr.Req.URL.Query().Get("hashid")
	if HashId == "" {
		JsonResponse(rr.W, "Get-параметр hashid не найден!")
	}
	res, err := i_.ItemService.ReadCacheDoc2(HashId)
	if err != nil {
		JsonResponse(rr.W, err.Error())
	}
	JsonResponse(rr.W, res)

}
