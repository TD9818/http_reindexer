package db_reindexer

import (
	"fmt"
	"main/internal/types"
	"net/url"

	"github.com/restream/reindexer"
)

func NewReindItem(dbCient *reindexer.Reindexer) *ReindItem {
	return &ReindItem{
		DBClient: dbCient,
	}
}

func (d_ *ReindItem) CreateDoc(empty_doc D, new_doc D, namespase string) (D, error) {
	// Create a new doc in the store.

	is_ins, err := d_.Is_conn_db(namespase, empty_doc).Insert(namespase, new_doc)
	if err != nil {
		return empty_doc, err
	}
	if is_ins == 1 {
		return new_doc, nil
	}
	return empty_doc, err
}

func (d_ *ReindItem) ReadDoc(docHashId string, empty_doc D, namespase string) (D, error) {
	// Чтение отдельного документа по DocHashId из БД.

	elem, found := d_.Is_conn(namespase, empty_doc).WhereString("Hashid", reindexer.EQ, docHashId).Get()
	if found {
		return elem, nil
	}
	return elem, fmt.Errorf("db_reindexer: ReadDoc: Документ с HashID = %v не найден!", docHashId)
}

func (d_ *ReindItem) ReadCacheDoc(docHashId string, empty_doc D, namespase string) (D, error) {
	// Чтение отдельного документа из кеша документов reindexer при заданном DocHashId.

	// ВНИМАНИЕ: при использовании AllowUnsafe(true)запросы возвращают общие указатели
	// на структуры в кеше объектов. Поэтому приложение НЕ ДОЛЖНО изменять возвращенные объекты.
	query := d_.Is_conn(namespase, empty_doc)
	elem, err := query.WhereString("Hashid", reindexer.EQ, docHashId).Exec().AllowUnsafe(true).FetchOne()

	if err == nil {
		return elem, nil
	}
	// Если в кэше еще нет результата, то выбирается обычным запросом.
	return d_.ReadDoc(docHashId, empty_doc, namespase)
}

func (d_ *ReindItem) UpdateDoc(doc D, empty_doc D, namespase string) (int, error) {

	is_updt, err := d_.Is_conn_db(namespase, empty_doc).Update(namespase, doc)
	if err != nil {
		return 0, err
	}
	return is_updt, err
}

func (d_ *ReindItem) DelDoc(docHashId string, empty_doc D, namespase string) (int, error) {
	// Delete doc in the store. Возвращает int - колличество
	// удаленных элементов или -1 - в случае ошибки

	query := d_.Is_conn(namespase, empty_doc)

	if docHashId != "all" {
		query = query.WhereString("Hashid", reindexer.EQ, docHashId)
	}
	quan, err := query.Delete()

	if err != nil {
		return -1, err
	}
	return quan, nil
}

func (d_ *ReindItem) DelDocAll(empty_doc D, namespase string) (int, error) {
	return d_.DelDoc("all", empty_doc, namespase)
}

func (d_ *ReindItem) ReadDocs0Page(http_query url.Values, namespace string, is_all bool) ([]types.Document_0, error) {

	var d_cond = &Doc_conditions_{}

	d_cond.GetDocConditions_(http_query)
	//  Если нужен полный список уровня 0
	if is_all {
		d_cond.Paginate0[1] = 0
	}
	query := d_cond.GetQueryTxt0_(d_.DBClient, namespace)

	// Чтение списка документов.
	iterator := query.Exec()
	defer iterator.Close()

	page := []types.Document_0{}
	for iterator.Next() {
		doc := *iterator.Object().(*types.Document_0)
		page = append(page, doc)
	}
	// Проверяем ошибки.
	if iterator.Error() != nil {
		return page, fmt.Errorf("ReadDocs0Page: При чтении документа 0-го уровня возникли ошибки\n")
	}
	return page, nil
}

func (d_ *ReindItem) ReadDocs1Page(http_query url.Values, namespace string, is_all bool) ([]types.Document_1, error) {

	var d_cond = &Doc_conditions_{}

	d_cond.GetDocConditions_(http_query)
	//  Если нужен полный список уровня 1
	if is_all {
		d_cond.Paginate1[1] = 0
	}
	query := d_cond.GetQueryTxt1_(d_.DBClient, namespace)

	// Чтение списка документов.
	iterator := query.Exec()
	defer iterator.Close()

	page := []types.Document_1{}
	for iterator.Next() {
		doc := *iterator.Object().(*types.Document_1)
		page = append(page, doc)
	}
	// Проверяем ошибки.
	if iterator.Error() != nil {
		return page, fmt.Errorf("ReadDocs1Page: При чтении документа 1-го уровня возникли ошибки\n")
	}
	return page, nil
}

func (d_ *ReindItem) ReadDocs2Page(http_query url.Values, namespace string, is_all bool) ([]types.Document_2, error) {

	var d_cond = &Doc_conditions_{}

	d_cond.GetDocConditions_(http_query)
	//  Если нужен полный список уровня 2
	if is_all {
		d_cond.Paginate2[1] = 0
	}
	query := d_cond.GetQueryTxt2_(d_.DBClient, namespace)

	// Чтение списка документов.
	iterator := query.Exec()
	defer iterator.Close()

	page := []types.Document_2{}
	for iterator.Next() {
		doc := *iterator.Object().(*types.Document_2)
		page = append(page, doc)
	}
	// Проверяем ошибки.
	if iterator.Error() != nil {
		return page, fmt.Errorf("ReadDocs0Page: При чтении документа 2-го уровня возникли ошибки\n")
	}
	return page, nil
}

func (d_ *ReindItem) readJoinDoc0(catHashId string, namespace_catalog string, namespace_docs string) (types.Document_0, error) {

	query := d_.Is_conn(namespace_catalog, types.Document_0{}).WhereString("Hashid", reindexer.EQ, catHashId).Limit(1)
	query = query.Join(d_.Is_conn(namespace_docs, types.Document_1{}), "Docs1list").On("Docs1hashidlist", reindexer.SET, "Hashid")
	res1, err := query.Exec().FetchOne()

	if err != nil {
		return types.Document_0{}, err
	}
	return *res1.(*types.Document_0), nil
}

func (d_ *ReindItem) readJoinDoc1(catHashId string, namespace_catalog string, namespace_docs string) (types.Document_1, error) {

	query := d_.Is_conn(namespace_catalog, types.Document_1{}).WhereString("Hashid", reindexer.EQ, catHashId).Limit(1)
	query = query.Join(d_.Is_conn(namespace_docs, types.Document_2{}), "Docs2list").On("Docs2hashidlist", reindexer.SET, "Hashid")
	res1, err := query.Exec().FetchOne()

	if err != nil {
		return types.Document_1{}, err
	}
	return *res1.(*types.Document_1), nil
}

func (d_ *ReindItem) readCacheJoinDoc0(HashId string, namespace_catalog string, namespace_docs string) (types.Document_0, error) {
	// Чтение отдельного каталога со связанными документами из кэша при заданном CatHashId.

	// ВНИМАНИЕ: при использовании AllowUnsafe(true)запросы возвращают общие указатели
	// на структуры в кеше объектов. Поэтому приложение НЕ ДОЛЖНО изменять возвращенные объекты.
	query := d_.Is_conn(namespace_catalog, types.Document_0{}).WhereString("Hashid", reindexer.EQ, HashId).Limit(1)
	query = query.Join(d_.Is_conn(namespace_docs, types.Document_1{}), "Docs1list").On("Docs1hashidlist", reindexer.SET, "Hashid")
	res, err := query.Exec().AllowUnsafe(true).FetchOne()

	if err != nil { // Если в кэше еще нет результата, то выбирается обычным запросом.
		return d_.readJoinDoc0(HashId, namespace_catalog, namespace_docs)
	}
	doc := *res.(*types.Document_0)
	return types.Document_0{
		Hashid:          doc.Hashid,
		Id:              doc.Id,
		Title:           doc.Title,
		Text:            doc.Text,
		Tags:            doc.Tags,
		Year:            doc.Year,
		Month:           doc.Month,
		Day:             doc.Day,
		Docs1hashidlist: doc.Docs1hashidlist,
		Docs1list:       doc.Docs1list,
	}, nil

}

func (d_ *ReindItem) readCacheJoinDoc1(HashId string, namespace_catalog string, namespace_docs string) (types.Document_1, error) {
	// Чтение отдельного каталога со связанными документами из кэша при заданном CatHashId.

	// ВНИМАНИЕ: при использовании AllowUnsafe(true)запросы возвращают общие указатели
	// на структуры в кеше объектов. Поэтому приложение НЕ ДОЛЖНО изменять возвращенные объекты.
	query := d_.Is_conn(namespace_catalog, types.Document_1{}).WhereString("Hashid", reindexer.EQ, HashId).Limit(1)
	query = query.Join(d_.Is_conn(namespace_docs, types.Document_2{}), "Docs2list").On("Docs2hashidlist", reindexer.SET, "Hashid")
	res, err := query.Exec().AllowUnsafe(true).FetchOne()

	if err != nil { // Если в кэше еще нет результата, то выбирается обычным запросом.
		return d_.readJoinDoc1(HashId, namespace_catalog, namespace_docs)
	}
	doc := *res.(*types.Document_1)
	return types.Document_1{
		Hashid:          doc.Hashid,
		Id:              doc.Id,
		Title:           doc.Title,
		Text:            doc.Text,
		Tags:            doc.Tags,
		Year:            doc.Year,
		Month:           doc.Month,
		Day:             doc.Day,
		Docs2hashidlist: doc.Docs2hashidlist,
		Docs2list:       doc.Docs2list,
	}, nil

}

func (d_ *ReindItem) ReadCacheJDoc1SortPage(HashId string, namespace_catalog string, namespace_docs string, http_query url.Values) (types.JDoc1SortPage, error) {

	doc, err := d_.readCacheJoinDoc1(HashId, namespace_catalog, namespace_docs)

	if err != nil {
		return types.JDoc1SortPage{types.Doc1Strip{}, []types.Document_2{}}, err
	}
	doc_strip := types.Doc1Strip{
		Hashid: doc.Hashid,
		Id:     doc.Id,
		Title:  doc.Title,
		Text:   doc.Text,
		Tags:   doc.Tags,
		Year:   doc.Year,
		Month:  doc.Month,
		Day:    doc.Day,
	}

	var d_cond = &Doc_conditions_{}

	type Document_sort_type struct {
		Hashid string   `json:"Hashid"          reindex:"Hashid,hash,pk"`
		Id     int      `json:"Id"              reindex:"Id"`
		Title  string   `json:"Title"           reindex:"Title"` //
		Text   string   `json:"Text"            reindex:"Text"`
		Tags   []string `json:"Tags"            reindex:"Tags"`
		Year   int      `json:"Year"            reindex:"Year"`
		Month  int      `json:"Month"           reindex:"Month"`
		Day    int      `json:"Day"             reindex:"Day"`
	}

	// Открываем новое пространство имен для одной сортировки
	d_.DBClient.OpenNamespace("temp_sort", reindexer.DefaultNamespaceOptions(), Document_sort_type{})

	for _, dc := range doc.Docs2list {
		// Трансформируем документы для пространства сортировки
		dc_transorm := Document_sort_type{
			Hashid: dc.Hashid,
			Id:     dc.Id,
			Title:  dc.Title,
			Text:   dc.Text,
			Tags:   dc.Tags,
			Year:   dc.Year,
			Month:  dc.Month,
			Day:    dc.Day,
		}
		err := d_.DBClient.Upsert("temp_sort", dc_transorm)
		if err != nil {
			return types.JDoc1SortPage{doc_strip, []types.Document_2{}}, err
		}
	}

	d_cond.GetDocConditions_(http_query)

	query := d_.Is_conn("temp_sort", Document_sort_type{})
	query = d_cond.JoinQueryTxt2_(d_.DBClient, query)

	iterator := query.Exec()
	defer iterator.Close()
	page_list_paginate := []types.Document_2{}
	for iterator.Next() {
		dc_transorm := *iterator.Object().(*Document_sort_type)
		doc := types.Document_2{
			Hashid: dc_transorm.Hashid,
			Id:     dc_transorm.Id,
			Title:  dc_transorm.Title,
			Text:   dc_transorm.Text,
			Tags:   dc_transorm.Tags,
			Year:   dc_transorm.Year,
			Month:  dc_transorm.Month,
			Day:    dc_transorm.Day,
		}
		page_list_paginate = append(page_list_paginate, doc)
	}
	// Обнуляем пространство имен
	d_.DBClient.DropNamespace("temp_sort")

	// Проверяем ошибки.
	if iterator.Error() != nil {
		return types.JDoc1SortPage{doc_strip, []types.Document_2{}}, err
	}

	return types.JDoc1SortPage{doc_strip, page_list_paginate}, nil
}

func (d_ *ReindItem) ReadCacheJDoc0SortPage(
	HashId string,
	namespace_catalog string,
	namespace_subcatalogs string,
	namespace_docs string,
	http_query url.Values) (types.JDoc0SortPage, error) {

	doc, err := d_.readCacheJoinDoc0(HashId, namespace_catalog, namespace_subcatalogs)

	if err != nil {
		return types.JDoc0SortPage{types.Doc0Strip{}, []types.JDoc1SortPage{}}, err
	}
	doc0_strip := types.Doc0Strip{
		Hashid: doc.Hashid,
		Id:     doc.Id,
		Title:  doc.Title,
		Text:   doc.Text,
		Tags:   doc.Tags,
		Year:   doc.Year,
		Month:  doc.Month,
		Day:    doc.Day,
	}

	var d_cond = &Doc_conditions_{}

	type Document_1_sort_type struct {
		Hashid string   `json:"Hashid"          reindex:"Hashid,hash,pk"`
		Id     int      `json:"Id"              reindex:"Id"`
		Title  string   `json:"Title"           reindex:"Title"` //
		Text   string   `json:"Text"            reindex:"Text"`
		Tags   []string `json:"Tags"            reindex:"Tags"`
		Year   int      `json:"Year"            reindex:"Year"`
		Month  int      `json:"Month"           reindex:"Month"`
		Day    int      `json:"Day"             reindex:"Day"`
	}

	// Открываем новое пространство имен для одной сортировки
	d_.DBClient.OpenNamespace("temp_sort_1", reindexer.DefaultNamespaceOptions(), Document_1_sort_type{})

	for _, dc := range doc.Docs1list {
		// Трансформируем документы для пространства сортировки
		dc_transorm_1 := Document_1_sort_type{
			Hashid: dc.Hashid,
			Id:     dc.Id,
			Title:  dc.Title,
			Text:   dc.Text,
			Tags:   dc.Tags,
			Year:   dc.Year,
			Month:  dc.Month,
			Day:    dc.Day,
		}
		err := d_.DBClient.Upsert("temp_sort_1", dc_transorm_1)
		if err != nil {
			return types.JDoc0SortPage{doc0_strip, []types.JDoc1SortPage{}}, err
		}
	}

	d_cond.GetDocConditions_(http_query)

	query := d_.Is_conn("temp_sort_1", Document_1_sort_type{})
	query = d_cond.JoinQueryTxt1_(d_.DBClient, query)

	iterator := query.Exec()
	defer iterator.Close()
	page_list_paginate := []types.JDoc1SortPage{}
	for iterator.Next() {
		dc_transorm_1 := *iterator.Object().(*Document_1_sort_type)

		res, err := d_.ReadCacheJDoc1SortPage(dc_transorm_1.Hashid, namespace_subcatalogs, namespace_docs, http_query)

		doc1_strip := types.Doc1Strip{
			Hashid: dc_transorm_1.Hashid,
			Id:     dc_transorm_1.Id,
			Title:  dc_transorm_1.Title,
			Text:   dc_transorm_1.Text,
			Tags:   dc_transorm_1.Tags,
			Year:   dc_transorm_1.Year,
			Month:  dc_transorm_1.Month,
			Day:    dc_transorm_1.Day,
		}
		if err != nil {
			page_list_paginate = append(page_list_paginate, types.JDoc1SortPage{doc1_strip, []types.Document_2{}})
		}
		page_list_paginate = append(page_list_paginate, types.JDoc1SortPage{doc1_strip, res.Doc2List})
	}
	// Обнуляем пространство имен
	d_.DBClient.DropNamespace("temp_sort_1")

	// Проверяем ошибки.
	if iterator.Error() != nil {
		return types.JDoc0SortPage{doc0_strip, []types.JDoc1SortPage{}}, err
	}
	return types.JDoc0SortPage{doc0_strip, page_list_paginate}, nil
}
