package db_reindexer

import (
	"main/internal/types"
	"net/url"
	"strconv"

	"github.com/restream/reindexer"
)

type D interface{}

type BDReind struct{}

type ResponseDoc struct {
	doc D
	err error
}

type ReindItem struct {
	DBClient *reindexer.Reindexer
}

// Проверка наличия пространства имен
func (d ReindItem) Is_conn(namespase string, interf interface{}) *reindexer.Query {
	// Проверка наличия пространства имен для запросов
	_, err := d.DBClient.DescribeNamespace(namespase)
	if err != nil {
		d.DBClient.OpenNamespace(namespase, reindexer.DefaultNamespaceOptions(), interf)
	}
	return d.DBClient.Query(namespase)
}

func (d ReindItem) Is_conn_db(namespase string, interf interface{}) *reindexer.Reindexer {
	// Проверка наличия пространства имен для действий с БД
	_, err := d.DBClient.DescribeNamespace(namespase)
	if err != nil {
		d.DBClient.OpenNamespace(namespase, reindexer.DefaultNamespaceOptions(), interf)
	}
	return d.DBClient
}

type Doc_conditions_ struct {
	Fieldrange     [3]string
	MinMax0        [2]int
	MinMax0Swich   [2]bool
	MinMax1        [2]int
	MinMax1Swich   [2]bool
	MinMax2        [2]int
	MinMax2Swich   [2]bool
	Sort0          [2]string
	Sort1          [2]string
	Sort2          [2]string
	Paginate0      [3]int
	Paginate0Swich [3]bool
	Paginate1      [3]int
	Paginate1Swich [3]bool
	Paginate2      [3]int
	Paginate2Swich [3]bool
}

func (d *Doc_conditions_) GetDocConditions_(query url.Values) *Doc_conditions_ {
	// Парсер Get-параметров запроса для сортировки, выборки и пагинации
	// Возможные параметры
	// Fieldrange0 str: Id, Year, Month, Day
	// Max0        int
	// Min0        int
	// Sort0       str: Id, Title, Text, Tags, Year, Month, Day
	// Order0      str: "+" или "-"
	// Lim_str0    int -1, 0, 1....
	// Pos0        int 0, 1,...
	// Page0       int 0, 1, 2,....
	// Индексы у полей могут быть: 0, 1, 2

	d.Fieldrange[0] = query.Get("Fieldrange0")
	d.MinMax0 = [2]int{0, 0}
	d.MinMax0Swich = [2]bool{false, false}
	if d.Fieldrange[0] != "" {
		res, err := strconv.Atoi(query.Get("Max0"))
		if err == nil {
			d.MinMax0[0] = res
			d.MinMax0Swich[0] = true
		}
		res, err = strconv.Atoi(query.Get("Min0"))
		if err == nil {
			d.MinMax0[1] = res
			d.MinMax0Swich[1] = true
		}
	}

	d.Fieldrange[1] = query.Get("Fieldrange1")
	d.MinMax1 = [2]int{0, 0}
	d.MinMax1Swich = [2]bool{false, false}
	if d.Fieldrange[1] != "" {
		res, err := strconv.Atoi(query.Get("Max1"))
		if err == nil {
			d.MinMax1[0] = res
			d.MinMax1Swich[0] = true
		}
		res, err = strconv.Atoi(query.Get("Min1"))
		if err == nil {
			d.MinMax1[1] = res
			d.MinMax1Swich[1] = true
		}
	}

	d.Fieldrange[2] = query.Get("Fieldrange2")
	d.MinMax2 = [2]int{0, 0}
	d.MinMax2Swich = [2]bool{false, false}
	if d.Fieldrange[2] != "" {
		res, err := strconv.Atoi(query.Get("Max2"))
		if err == nil {
			d.MinMax2[0] = res
			d.MinMax2Swich[0] = true
		}
		res, err = strconv.Atoi(query.Get("Min2"))
		if err == nil {
			d.MinMax2[1] = res
			d.MinMax2Swich[1] = true
		}
	} else {
	}

	d.Sort0[0] = query.Get("Sort0")
	d.Sort0[1] = query.Get("Order0")
	if d.Sort0[1] != "-" {
		d.Sort0[1] = "+"
	}

	d.Sort1[0] = query.Get("Sort1")
	d.Sort1[1] = query.Get("Order1")
	if d.Sort1[1] != "-" {
		d.Sort1[1] = "+"
	}

	d.Sort2[0] = query.Get("Sort2")
	d.Sort2[1] = query.Get("Order2")
	if d.Sort2[1] != "-" {
		d.Sort2[1] = "+"
	}

	res, err := strconv.Atoi(query.Get("Lim_str0"))
	d.Paginate0 = [3]int{0, 0, 0}
	d.Paginate0Swich = [3]bool{false, false, false}
	if err == nil {
		d.Paginate0[0] = res
		d.Paginate0Swich[0] = true
		res, err := strconv.Atoi(query.Get("Pos0"))
		if err == nil {
			d.Paginate0[1] = res
			d.Paginate0Swich[1] = true
		}
		res, err = strconv.Atoi(query.Get("Page0"))
		if err == nil {
			d.Paginate0[2] = res
			d.Paginate0Swich[2] = true
		}
	}

	res, err = strconv.Atoi(query.Get("Lim_str1"))
	d.Paginate1 = [3]int{0, 0, 0}
	d.Paginate1Swich = [3]bool{false, false, false}
	if err == nil {
		d.Paginate1[0] = res
		d.Paginate1Swich[0] = true
		res, err := strconv.Atoi(query.Get("Pos1"))
		if err == nil {
			d.Paginate1[1] = res
			d.Paginate1Swich[1] = true
		}
		res, err = strconv.Atoi(query.Get("Page1"))
		if err == nil {
			d.Paginate1[2] = res
			d.Paginate1Swich[2] = true
		}
	}

	res, err = strconv.Atoi(query.Get("Lim_str2"))
	d.Paginate2 = [3]int{0, 0, 0}
	d.Paginate2Swich = [3]bool{false, false, false}
	if err == nil {
		d.Paginate2[0] = res
		d.Paginate2Swich[0] = true
		res, err := strconv.Atoi(query.Get("Pos2"))
		if err == nil {
			d.Paginate2[1] = res
			d.Paginate2Swich[1] = true
		}
		res, err = strconv.Atoi(query.Get("Page2"))
		if err == nil {
			d.Paginate2[2] = res
			d.Paginate2Swich[2] = true
		}
	}
	return d
}

func (d *Doc_conditions_) GetQueryTxt0_(DB *reindexer.Reindexer, namespace string) *reindexer.Query {

	query := ReindItem{DB}.Is_conn(namespace, types.Document_0{})

	if d.Fieldrange[0] != "" {
		if d.MinMax0Swich[0] {
			query = query.WhereInt(d.Fieldrange[0], reindexer.GE, d.MinMax0[0])
		}
		if d.MinMax0Swich[1] {
			query = query.WhereInt(d.Fieldrange[0], reindexer.LT|reindexer.EQ, d.MinMax0[1])
		}
	}
	if d.Sort0[0] != "" {
		if d.Sort0[1] == "+" {
			query = query.Sort(d.Sort0[0], false)
		} else {
			query = query.Sort(d.Sort0[0], true)
		}
	}
	if d.Paginate0Swich[0] {
		if d.Paginate0[0] > 0 {
			query = query.Limit(d.Paginate0[0])
			if d.Paginate0Swich[1] {
				query = query.Offset(d.Paginate0[1])
			} else if d.Paginate0Swich[2] {
				query = query.Offset(d.Paginate0[2] * d.Paginate0[0])
			} else {
				query = query.Offset(0)
			}
		}
	}
	return query
}

func (d *Doc_conditions_) GetQueryTxt1_(DB *reindexer.Reindexer, namespace string) *reindexer.Query {

	query := ReindItem{DB}.Is_conn(namespace, types.Document_1{})

	if d.Fieldrange[1] != "" {
		if d.MinMax1Swich[0] {
			query = query.WhereInt(d.Fieldrange[1], reindexer.GE, d.MinMax1[0])
		}
		if d.MinMax1Swich[1] {
			query = query.WhereInt(d.Fieldrange[1], reindexer.LT|reindexer.EQ, d.MinMax1[1])
		}
	}
	if d.Sort1[0] != "" {
		if d.Sort1[1] == "+" {
			query = query.Sort(d.Sort1[0], false)
		} else {
			query = query.Sort(d.Sort1[0], true)
		}
	}
	if d.Paginate1Swich[0] {
		if d.Paginate1[0] > 0 {
			query = query.Limit(d.Paginate1[0])
			if d.Paginate1Swich[1] {
				query = query.Offset(d.Paginate1[1])
			} else if d.Paginate1Swich[2] {
				query = query.Offset(d.Paginate1[2] * d.Paginate1[0])
			} else {
				query = query.Offset(0)
			}
		}
	}
	return query
}

func (d *Doc_conditions_) GetQueryTxt2_(DB *reindexer.Reindexer, namespace string) *reindexer.Query {

	query := ReindItem{DB}.Is_conn(namespace, types.Document_2{})

	if d.Fieldrange[2] != "" {
		if d.MinMax2Swich[0] {
			query = query.WhereInt(d.Fieldrange[2], reindexer.GE, d.MinMax2[0])
		}
		if d.MinMax2Swich[1] {
			query = query.WhereInt(d.Fieldrange[2], reindexer.LT|reindexer.EQ, d.MinMax2[1])
		}
	}
	if d.Sort2[0] != "" {
		if d.Sort2[1] == "+" {
			query = query.Sort(d.Sort2[0], false)
		} else {
			query = query.Sort(d.Sort2[0], true)
		}
	}
	if d.Paginate2Swich[0] {
		if d.Paginate2[0] > 0 {
			query = query.Limit(d.Paginate2[0])
			if d.Paginate2Swich[1] {
				query = query.Offset(d.Paginate2[1])
			} else if d.Paginate2Swich[2] {
				query = query.Offset(d.Paginate2[2] * d.Paginate2[0])
			} else {
				query = query.Offset(0)
			}
		}
	}
	return query
}

func (d *Doc_conditions_) JoinQueryTxt1_(DB *reindexer.Reindexer, query *reindexer.Query) *reindexer.Query {

	if d.Fieldrange[1] != "" {
		if d.MinMax1Swich[0] {
			query = query.WhereInt(d.Fieldrange[1], reindexer.GE, d.MinMax1[0])
		}
		if d.MinMax1Swich[1] {
			query = query.WhereInt(d.Fieldrange[1], reindexer.LT|reindexer.EQ, d.MinMax1[1])
		}
	}
	if d.Sort1[0] != "" {
		if d.Sort1[1] == "+" {
			query = query.Sort(d.Sort1[0], false)
		} else {
			query = query.Sort(d.Sort1[0], true)
		}
	}
	if d.Paginate1Swich[0] {
		if d.Paginate1[0] > 0 {
			query = query.Limit(d.Paginate1[0])
			if d.Paginate1Swich[1] {
				query = query.Offset(d.Paginate1[1])
			} else if d.Paginate1Swich[2] {
				query = query.Offset(d.Paginate1[2] * d.Paginate1[0])
			} else {
				query = query.Offset(0)
			}
		}
	}
	return query
}

func (d *Doc_conditions_) JoinQueryTxt2_(DB *reindexer.Reindexer, query *reindexer.Query) *reindexer.Query {

	if d.Fieldrange[2] != "" {
		if d.MinMax2Swich[0] {
			query = query.WhereInt(d.Fieldrange[2], reindexer.GE, d.MinMax2[0])
		}
		if d.MinMax2Swich[1] {
			query = query.WhereInt(d.Fieldrange[2], reindexer.LT|reindexer.EQ, d.MinMax2[1])
		}
	}
	if d.Sort2[0] != "" {
		if d.Sort2[1] == "+" {
			query = query.Sort(d.Sort2[0], false)
		} else {
			query = query.Sort(d.Sort2[0], true)
		}
	}
	if d.Paginate2Swich[0] {
		if d.Paginate2[0] > 0 {
			query = query.Limit(d.Paginate2[0])
			if d.Paginate2Swich[1] {
				query = query.Offset(d.Paginate2[1])
			} else if d.Paginate2Swich[2] {
				query = query.Offset(d.Paginate2[2] * d.Paginate2[0])
			} else {
				query = query.Offset(0)
			}
		}
	}
	return query
}

// Старый, рабочий, но менее понятный вариант

// type Doc_conditions struct {
// 	Fieldrange [3]string
// 	MinMax0    [4]int
// 	MinMax1    [4]int
// 	MinMax2    [4]int
// 	Sort0      [2]string
// 	Sort1      [2]string
// 	Sort2      [2]string
// 	Paginate0  [6]int
// 	Paginate1  [6]int
// 	Paginate2  [6]int
// }

// func (d *Doc_conditions) GetDocConditions(query url.Values) *Doc_conditions {
// 	// Парсер Get-параметров запроса для сортировки, выборки и пагинации
// 	// Возможные параметры
// 	// Fieldrange0 str: Id, Year, Month, Day
// 	// Max0        int
// 	// Min0        int
// 	// Sort0       str: Id, Title, Text, Tags, Year, Month, Day
// 	// Order0      str: "+" или "-"
// 	// Lim_str0    int -1, 0, 1....
// 	// Pos0        int 0, 1,...
// 	// Page0       int 0, 1, 2,....
// 	// Индексы у полей могут быть: 0, 1, 2

// 	d.Fieldrange[0] = query.Get("Fieldrange0")
// 	if d.Fieldrange[0] != "" {
// 		res, err := strconv.Atoi(query.Get("Max0"))
// 		if err == nil {
// 			d.MinMax0[0] = res
// 			d.MinMax0[1] = 1
// 		} else {
// 			d.MinMax0[0] = 0
// 			d.MinMax0[1] = 0
// 		}
// 		res, err = strconv.Atoi(query.Get("Min0"))
// 		if err == nil {
// 			d.MinMax0[2] = res
// 			d.MinMax0[3] = 1
// 		} else {
// 			d.MinMax0[2] = 0
// 			d.MinMax0[3] = 0
// 		}
// 	} else {
// 		d.MinMax0 = [4]int{0, 0, 0, 0}
// 	}

// 	d.Fieldrange[1] = query.Get("Fieldrange1")
// 	if d.Fieldrange[1] != "" {
// 		res, err := strconv.Atoi(query.Get("Max1"))
// 		if err == nil {
// 			d.MinMax1[0] = res
// 			d.MinMax1[1] = 1
// 		} else {
// 			d.MinMax1[0] = 0
// 			d.MinMax1[1] = 0
// 		}
// 		res, err = strconv.Atoi(query.Get("Min1"))
// 		if err == nil {
// 			d.MinMax1[2] = res
// 			d.MinMax1[3] = 1
// 		} else {
// 			d.MinMax1[2] = 0
// 			d.MinMax1[3] = 0
// 		}
// 	} else {
// 		d.MinMax1 = [4]int{0, 0, 0, 0}
// 	}

// 	d.Fieldrange[2] = query.Get("Fieldrange2")
// 	if d.Fieldrange[2] != "" {
// 		res, err := strconv.Atoi(query.Get("Max2"))
// 		if err == nil {
// 			d.MinMax2[0] = res
// 			d.MinMax2[1] = 1
// 		} else {
// 			d.MinMax2[0] = 0
// 			d.MinMax2[1] = 0
// 		}
// 		res, err = strconv.Atoi(query.Get("Min2"))
// 		if err == nil {
// 			d.MinMax2[2] = res
// 			d.MinMax2[3] = 1
// 		} else {
// 			d.MinMax2[2] = 0
// 			d.MinMax2[3] = 0
// 		}
// 	} else {
// 		d.MinMax2 = [4]int{0, 0, 0, 0}
// 	}

// 	d.Sort0[0] = query.Get("Sort0")
// 	d.Sort0[1] = query.Get("Order0")
// 	if d.Sort0[1] != "-" {
// 		d.Sort0[1] = "+"
// 	}

// 	d.Sort1[0] = query.Get("Sort1")
// 	d.Sort1[1] = query.Get("Order1")
// 	if d.Sort1[1] != "-" {
// 		d.Sort1[1] = "+"
// 	}

// 	d.Sort2[0] = query.Get("Sort2")
// 	d.Sort2[1] = query.Get("Order2")
// 	if d.Sort2[1] != "-" {
// 		d.Sort2[1] = "+"
// 	}

// 	res, err := strconv.Atoi(query.Get("Lim_str0"))
// 	if err == nil {
// 		d.Paginate0[0] = res
// 		d.Paginate0[1] = 1
// 		res, err := strconv.Atoi(query.Get("Pos0"))
// 		if err == nil {
// 			d.Paginate0[2] = res
// 			d.Paginate0[3] = 1
// 		} else {
// 			d.Paginate0[2] = 0
// 			d.Paginate0[3] = 0
// 		}
// 		res, err = strconv.Atoi(query.Get("Page0"))
// 		if err == nil {
// 			d.Paginate0[4] = res
// 			d.Paginate0[5] = 1
// 		} else {
// 			d.Paginate0[4] = 0
// 			d.Paginate0[5] = 0
// 		}
// 	} else {
// 		d.Paginate0 = [6]int{0, 0, 0, 0, 0, 0}
// 	}

// 	res, err = strconv.Atoi(query.Get("Lim_str1"))
// 	if err == nil {
// 		d.Paginate1[0] = res
// 		d.Paginate1[1] = 1
// 		res, err := strconv.Atoi(query.Get("Pos1"))
// 		if err == nil {
// 			d.Paginate1[2] = res
// 			d.Paginate1[3] = 1
// 		} else {
// 			d.Paginate1[2] = 0
// 			d.Paginate1[3] = 0
// 		}
// 		res, err = strconv.Atoi(query.Get("Page1"))
// 		if err == nil {
// 			d.Paginate1[4] = res
// 			d.Paginate1[5] = 1
// 		} else {
// 			d.Paginate1[4] = 0
// 			d.Paginate1[5] = 0
// 		}
// 	} else {
// 		d.Paginate1 = [6]int{0, 0, 0, 0, 0, 0}
// 	}

// 	res, err = strconv.Atoi(query.Get("Lim_str2"))
// 	if err == nil {
// 		d.Paginate2[0] = res
// 		d.Paginate2[1] = 1
// 		res, err := strconv.Atoi(query.Get("Pos2"))
// 		if err == nil {
// 			d.Paginate2[2] = res
// 			d.Paginate2[3] = 1
// 		} else {
// 			d.Paginate2[2] = 0
// 			d.Paginate2[3] = 0
// 		}
// 		res, err = strconv.Atoi(query.Get("Page2"))
// 		if err == nil {
// 			d.Paginate2[4] = res
// 			d.Paginate2[5] = 1
// 		} else {
// 			d.Paginate2[4] = 0
// 			d.Paginate2[5] = 0
// 		}
// 	} else {
// 		d.Paginate2 = [6]int{0, 0, 0, 0, 0, 0}
// 	}

// 	return d
// }

// func (d *Doc_conditions) GetQueryTxt0(DB *reindexer.Reindexer, namespace string) *reindexer.Query {

// 	query := ReindItem{DB}.Is_conn(namespace, types.Document_0{})

// 	if d.Fieldrange[0] != "" {
// 		if d.MinMax0[1] != 0 {
// 			query = query.WhereInt(d.Fieldrange[0], reindexer.GE, d.MinMax0[0])
// 		}
// 		if d.MinMax0[3] != 0 {
// 			query = query.WhereInt(d.Fieldrange[0], reindexer.LT|reindexer.EQ, d.MinMax0[2])
// 		}
// 	}
// 	if d.Sort0[0] != "" {
// 		if d.Sort0[1] == "+" {
// 			query = query.Sort(d.Sort0[0], false)
// 		} else {
// 			query = query.Sort(d.Sort0[0], true)
// 		}
// 	}
// 	if d.Paginate0[1] != 0 {
// 		if d.Paginate0[0] > 0 {
// 			query = query.Limit(d.Paginate0[0])
// 			if d.Paginate0[3] != 0 {
// 				query = query.Offset(d.Paginate0[2])
// 			} else if d.Paginate0[5] != 0 {
// 				query = query.Offset(d.Paginate0[4] * d.Paginate0[0])
// 			} else {
// 				query = query.Offset(0)
// 			}
// 		}
// 	}
// 	return query
// }

// func (d *Doc_conditions) GetQueryTxt1(DB *reindexer.Reindexer, namespace string) *reindexer.Query {

// 	query := ReindItem{DB}.Is_conn(namespace, types.Document_1{})

// 	if d.Fieldrange[1] != "" {
// 		if d.MinMax1[1] != 0 {
// 			query = query.WhereInt(d.Fieldrange[1], reindexer.GE, d.MinMax1[0])
// 		}
// 		if d.MinMax1[3] != 0 {
// 			query = query.WhereInt(d.Fieldrange[1], reindexer.LT|reindexer.EQ, d.MinMax1[2])
// 		}
// 	}
// 	if d.Sort1[0] != "" {
// 		if d.Sort1[1] == "+" {
// 			query = query.Sort(d.Sort1[0], false)
// 		} else {
// 			query = query.Sort(d.Sort1[0], true)
// 		}
// 	}
// 	if d.Paginate1[1] != 0 {
// 		if d.Paginate1[0] > 0 {
// 			query = query.Limit(d.Paginate1[0])
// 			if d.Paginate1[3] != 0 {
// 				query = query.Offset(d.Paginate1[2])
// 			} else if d.Paginate1[5] != 0 {
// 				query = query.Offset(d.Paginate1[4] * d.Paginate1[0])
// 			} else {
// 				query = query.Offset(0)
// 			}
// 		}
// 	}
// 	return query
// }

// func (d *Doc_conditions) GetQueryTxt2(DB *reindexer.Reindexer, namespace string) *reindexer.Query {

// 	query := ReindItem{DB}.Is_conn(namespace, types.Document_2{})

// 	if d.Fieldrange[2] != "" {
// 		if d.MinMax2[1] != 0 {
// 			query = query.WhereInt(d.Fieldrange[2], reindexer.GE, d.MinMax2[0])
// 		}
// 		if d.MinMax2[3] != 0 {
// 			query = query.WhereInt(d.Fieldrange[2], reindexer.LT|reindexer.EQ, d.MinMax2[2])
// 		}
// 	}
// 	if d.Sort2[0] != "" {
// 		if d.Sort2[1] == "+" {
// 			query = query.Sort(d.Sort2[0], false)
// 		} else {
// 			query = query.Sort(d.Sort2[0], true)
// 		}
// 	}
// 	if d.Paginate2[1] != 0 {
// 		if d.Paginate2[0] > 0 {
// 			query = query.Limit(d.Paginate2[0])
// 			if d.Paginate2[3] != 0 {
// 				query = query.Offset(d.Paginate2[2])
// 			} else if d.Paginate2[5] != 0 {
// 				query = query.Offset(d.Paginate2[4] * d.Paginate2[0])
// 			} else {
// 				query = query.Offset(0)
// 			}
// 		}
// 	}
// 	return query
// }

// func (d *Doc_conditions) JoinQueryTxt1(DB *reindexer.Reindexer, query *reindexer.Query) *reindexer.Query {

// 	if d.Fieldrange[1] != "" {
// 		if d.MinMax1[1] != 0 {
// 			query = query.WhereInt(d.Fieldrange[1], reindexer.GE, d.MinMax1[0])
// 		}
// 		if d.MinMax1[3] != 0 {
// 			query = query.WhereInt(d.Fieldrange[1], reindexer.LT|reindexer.EQ, d.MinMax1[2])
// 		}
// 	}
// 	if d.Sort1[0] != "" {
// 		if d.Sort1[1] == "+" {
// 			query = query.Sort(d.Sort1[0], false)
// 		} else {
// 			query = query.Sort(d.Sort1[0], true)
// 		}
// 	}
// 	if d.Paginate1[1] != 0 {
// 		if d.Paginate1[0] > 0 {
// 			query = query.Limit(d.Paginate1[0])
// 			if d.Paginate1[3] != 0 {
// 				query = query.Offset(d.Paginate1[2])
// 			} else if d.Paginate1[5] != 0 {
// 				query = query.Offset(d.Paginate1[4] * d.Paginate1[0])
// 			} else {
// 				query = query.Offset(0)
// 			}
// 		}
// 	}
// 	return query
// }

// func (d *Doc_conditions) JoinQueryTxt2(DB *reindexer.Reindexer, query *reindexer.Query) *reindexer.Query {

// 	if d.Fieldrange[2] != "" {
// 		if d.MinMax2[1] != 0 {
// 			query = query.WhereInt(d.Fieldrange[2], reindexer.GE, d.MinMax2[0])
// 		}
// 		if d.MinMax2[3] != 0 {
// 			query = query.WhereInt(d.Fieldrange[2], reindexer.LT|reindexer.EQ, d.MinMax2[2])
// 		}
// 	}
// 	if d.Sort2[0] != "" {
// 		if d.Sort2[1] == "+" {
// 			query = query.Sort(d.Sort2[0], false)
// 		} else {
// 			query = query.Sort(d.Sort2[0], true)
// 		}
// 	}
// 	if d.Paginate2[1] != 0 {
// 		if d.Paginate2[0] > 0 {
// 			query = query.Limit(d.Paginate2[0])
// 			if d.Paginate2[3] != 0 {
// 				query = query.Offset(d.Paginate2[2])
// 			} else if d.Paginate2[5] != 0 {
// 				query = query.Offset(d.Paginate2[4] * d.Paginate2[0])
// 			} else {
// 				query = query.Offset(0)
// 			}
// 		}
// 	}
// 	return query
// }
