// https://github.com/shehab-as/Go-Microservices/tree/main/pkg

package db_reindexer

import (
	"fmt"

	"main/internal/globals"
	"main/internal/types"

	"github.com/restream/reindexer"
)

var IS_DOCKER bool = globals.IS_DOCKER
var DOCKER_CONTAINER string = globals.DOCKER_CONTAINER

func OpenNamespace(db *reindexer.Reindexer, namespase string, doc D) {

	default_namespase := reindexer.DefaultNamespaceOptions()
	db.OpenNamespace(namespase, default_namespase, doc)
}

func New() *reindexer.Reindexer {
	/* Инициализируем БД */

	var db *reindexer.Reindexer

	if IS_DOCKER {
		// Вариант указания хоста для docker-контейнера
		db = reindexer.NewReindex("cproto://"+DOCKER_CONTAINER+":6534/testdb", reindexer.WithCreateDBIfMissing())
		fmt.Printf("Host: %v, Port: 6534\n", DOCKER_CONTAINER)
	} else {
		// Вариант указания хоста для запуска с машины без контейнера
		db = reindexer.NewReindex("cproto://127.0.0.1:6534/testdb", reindexer.WithCreateDBIfMissing())
		fmt.Println("Host: 127.0.0.1, Port: 6534")
	}

	OpenNamespace(db, globals.DOC0NAMESPACE, types.Document_0{})
	OpenNamespace(db, globals.DOC1NAMESPACE, types.Document_1{})
	OpenNamespace(db, globals.DOC2NAMESPACE, types.Document_2{})

	return db
}
