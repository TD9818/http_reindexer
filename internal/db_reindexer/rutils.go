package db_reindexer

import (
	"crypto/md5"
	"fmt"

	"main/internal/globals"

	"strings"
)

var IS_REPORT bool = globals.IS_REPORT

func Msg(msg string) {
	if IS_REPORT {
		fmt.Println(msg)
	}
}

func Is_err(err error, msg string) {
	if err != nil {
		fmt.Println(msg)
		panic(err)
	}
}

func MD5(data string) string {
	// MD5 - Превращает содержимое из переменной data в md5-хеш

	h := md5.Sum([]byte(data))
	return fmt.Sprintf("%x", h)
}

func Str_to_mass(s string) []string {

	s_ := strings.Trim(s, "]")
	s_ = strings.Trim(s_, "[")
	mass := []string{}
	for _, item := range strings.Split(s_, ",") {
		mass = append(mass, strings.Trim(item, " "))
	}
	return mass
}

func Is_str_in_mass(s string, mass []string) bool {
	// Возвращает true, если s есть в массиве строк mass, иначе возвращает false.

	res := false
	for _, vol := range mass {
		if s == vol {
			break
		}
	}
	return res
}

func Is_int_in_mass(s int, mass []int) bool {
	// Возвращает true, если s есть в массиве чисел mass, иначе возвращает false.

	res := false
	for _, vol := range mass {
		if s == vol {
			break
		}
	}
	return res
}

//type I interface{}

func Keys_str_str_map(i map[string]string) []string {
	keys_list := []string{}
	for key, _ := range i {
		keys_list = append(keys_list, key)
	}
	return keys_list
}
func Keys_str_int_map(i map[string]int) []string {
	keys_list := []string{}
	for key, _ := range i {
		keys_list = append(keys_list, key)
	}
	return keys_list
}
