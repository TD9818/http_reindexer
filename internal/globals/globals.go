package globals

import (
	"fmt"
	"io/ioutil"
	"net/http"

	"gopkg.in/yaml.v2"
)

type Conf struct {
	Is_docker        bool   `yaml:"is_docker"`
	Is_report        bool   `yaml:"is_report"`
	Docker_container string `yaml:"docker_container"`
	Doc0namespace    string `yaml:"doc0namespace"`
	Doc1namespace    string `yaml:"doc1namespace"`
	Doc2namespace    string `yaml:"doc2namespace"`
}

const PATH = "config.yaml"

// const PATH = "main/config/config.yaml"

//const PATH = "/internal/reindexer/conf.yaml"

func (c *Conf) GetConf(w http.ResponseWriter) *Conf {

	yamlFile, err := ioutil.ReadFile(PATH)

	if err != nil {
		msg := fmt.Sprintf("yamlFile.Get err   #%v ", err.Error())
		http.Error(w, msg, http.StatusMethodNotAllowed)
	}

	err = yaml.Unmarshal(yamlFile, c)

	if err != nil {
		msg := fmt.Sprintf("yaml.Unmarshal: %v", err.Error())
		http.Error(w, msg, http.StatusMethodNotAllowed)
	}
	return c
}

func (c *Conf) GetTestYamlToGlobals(path string) *Conf {

	yamlFile, err := ioutil.ReadFile(path)

	if err != nil {
		msg := fmt.Sprintf("yamlFile.Get err   #%v ", err.Error())
		fmt.Println(msg)
	}

	err = yaml.Unmarshal(yamlFile, c)

	if err != nil {
		msg := fmt.Sprintf("yaml.Unmarshal: %v", err.Error())
		fmt.Println(msg)
	}
	return c
}

var c Conf
var IS_DOCKER bool = c.GetTestYamlToGlobals(PATH).Is_docker
var IS_REPORT bool = c.GetTestYamlToGlobals(PATH).Is_report
var DOCKER_CONTAINER string = c.GetTestYamlToGlobals(PATH).Docker_container

var DOC0NAMESPACE string = c.GetTestYamlToGlobals(PATH).Doc0namespace
var DOC1NAMESPACE string = c.GetTestYamlToGlobals(PATH).Doc1namespace
var DOC2NAMESPACE string = c.GetTestYamlToGlobals(PATH).Doc2namespace
